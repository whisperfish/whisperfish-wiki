# Emojis

Whisperfish supports different colored emoji styles. If no specific style is
installed and selected, emojis will be shown using the black-and-white emoji
font provided by the system.


## 1. Downloading emoji styles

Use the [`emoji-dl.sh`](emoji-dl.sh) script to download one of the supported
emoji styles. Run the following commands in a command line terminal on your
computer (*not* on your phone).

First, select an emoji style from the table below, and enter the folder into
which you downloaded the script. Then, run the script with the style key (see
column "Key" in the table) as argument to download the emoji set.

```bash
# 1. enter the folder into which you downloaded the script
cd ~/Downloads

# 2. either pick a style from the table below, or use the --help option to
#    get a list of supported emoji sets
./emoji-dl.sh --help

# 3. download the emoji set like this:
# emoji-dl.sh <style>, with <style> being the the one you selected:
./emoji-dl.sh openmoji
```

Note: if you choose a *raster* style instead of a vector style (cf. table below),
the script runs some heavy post-processing to compress the images. Do not attempt
to run this on your phone, as it is resource intensive and takes quite some time
even when run on a decent computer.


## 2. Installing downloaded styles

The [`emoji-dl.sh`](emoji-dl.sh) script places downloaded files in the
`sailor-emoji` folder in your current working directory.

If everything finished successfully, you can remove the temporary folder
`sailor-emoji-temporary`. Otherwise, you could use the files there to diagnose
the problem.

Copy or move the `sailor-emoji` folder to your Sailfish device into
`~/.local/share/sailor-emoji`. The folder structure should look similar to this
now:

    /home/nemo/.local/share/sailor-emoji/openmoji/15.0.0/<many files>.svg
          |                              |        |                   └ the file extension
          |                              |        |                     depends on the style
          |                              |        └ this depends on the style version
          |                              └ this is the key (i.e. name) of the style you picked
          └ this will be either "nemo" or "defaultuser"

Note: copying a full emoji set with all ~9000 files will take some time.


### Emoji styles

| Name             | Key                      | Format   | License         | Example | Source |
|------------------|--------------------------|----------|-----------------|---------|--------|
| OpenMoji         | `openmoji`               | vector   | CC-BY-SA 4.0    | ![](https://em-content.zobj.net/thumbs/72/openmoji/338/partying-face_1f973.png) | [Github](https://github.com/hfg-gmuend/openmoji/), [Emojipedia](https://emojipedia.org/openmoji)
| Twemoji          | `twemoji`                | vector   | CC-BY-SA 4.0    | ![](https://em-content.zobj.net/thumbs/72/twitter/322/partying-face_1f973.png) | [Github](https://github.com/jdecked/twemoji/releases), [Emojipedia](https://emojipedia.org/twitter)
| Microsoft (glossy) | `microsoft-3D-fluent`  | raster   | proprietary     | ![](https://em-content.zobj.net/thumbs/72/microsoft-3D-fluent/406/partying-face_1f973.png) | [Emojipedia](https://emojipedia.org/twitter-emoji-stickers)
| Google Noto      | `google`                 | raster   | OpenFontLicense | ![](https://em-content.zobj.net/thumbs/72/google/350/partying-face_1f973.png) | [Emojipedia](https://emojipedia.org/google)
| WhatsApp         | `whatsapp`               | raster   | proprietary     | ![](https://em-content.zobj.net/thumbs/72/whatsapp/326/partying-face_1f973.png) | [Emojipedia](https://emojipedia.org/whatsapp)
| Apple            | `apple`                  | raster   | proprietary     | ![](https://em-content.zobj.net/thumbs/72/apple/325/partying-face_1f973.png) | [Emojipedia](https://emojipedia.org/apple)

Example emoji in all styles: [Emojipedia](https://emojipedia.org/partying-face/).


## 3a. Enabling a style in the patched emoji keyboard

This section is only relevant if you installed the
[patched emoji keyboard](https://openrepos.net/content/ichthyosaurus/patch-stock-emoji-keyboard-colors)
from OpenRepos or the Patchmanager web catalog.

Switch to the emoji keyboard and tap the "gear" icon in the lower right corner.
Then select the style you want to use from the list.

After changing the style, it is sometimes necessary to switch to a normal
keyboard and then back to the emoji keyboard to make sure all keys follow the
new style.

Note: all styles you have installed should be marked as "installed" in the list.
If this is not the case, make sure you have downloaded the right version and
placed it into the correct folder (see above).

Note for Whisperfish: changing the emoji set to another vector (SVG) set is effective
immediately. Changing from one raster (PNG) set to another requires restarting
Whisperfish. This is because the system caches the emoji image files you already viewed.
This results to some emojis being from the old set, others from the new set.


## 3b. Enabling a style manually

Follow this section to select a style for all `sailor-emoji`-enabled apps, like
Whisperfish and the patched emoji keyboard.

Run these commands in a terminal on your Sailfish device.

Check the current style: this may print something like `'twemoji'`, or it will
be empty if you never enabled a style before.

```bash
# 1. check the current style:
dconf read /apps/sailor-emoji/currentStyle
```

Then enable a style. The name you have to use here is the same as the name used
for downloading, as well as the folder name. See the "Key" column in the table
above.

> Important: the key must be enclosed in two sets of quotation marks. Otherwise
> it will not work.
>
> Correct: `'"selected-key"'`
>
> Wrong:   `"selected-key"` or `'selected-key'` etc.

Note: you cannot select a specific version this way, you must provide only the
key of the style. The latest installed version will be used automatically.

```bash
# 2. select a style: use your selected key instead of "openmoji" below
dconf write /apps/sailor-emoji/currentStyle '"openmoji"'
```

Last but not least, check if the value was updated correctly.

```bash
# 3. check the updated style:
dconf read /apps/sailor-emoji/currentStyle
# prints e.g. 'openmoji'
```

Note for Whisperfish: changing the emoji set to another vector (SVG) set is effective
immediately. Changing from one raster (PNG) set to another requires restarting
Whisperfish. This is because the system caches the emoji image files you already viewed.
This results to some emojis being from the old set, others from the new set.


## Tips

### Whisperfish doesn't start

Make sure the `dconf` key points to an entry name that exists in `emoji.js`, and
make sure the corresponding emoji folder exists.

If you use the patched emoji keyboard, open its settings and switch once back
and forth between two styles. You can use the Notes app to see the keyboard.


### Emojis use a lot of space

If you are low on disk space, consider using vector emojis (SVG) instead of
raster emojis (PNG). See the "Format" column in the overview table.

Vector emojis need 8-12 MiB on disk, raster emojis need 20-40 MiB. If you skip
the compression step for raster emojis in [`emoji-dl.sh`](emoji-dl.sh), then
they need 2-3x more space. You can use the `--no-shrink` and `--no-crush` options
to disable all post-processing.

All styles need about 170 MiB in 50000 files. It is not recommended to install
all styles at once.


## Developers


### Adding support for a new version

The [`emoji-dl.sh`](emoji-dl.sh) script and the
[`emoji.js`](https://gitlab.com/whisperfish/whisperfish/-/blob/master/qml/js/emoji.js)
module have to be updated when a new Emoji / Unicode version is released.

1. Update the regex:
    1. open `emoji.js` and search for "`// RegExp for Emoji`"
    2. open https://github.com/twitter/twemoji-parser/releases/latest and click
       on the "tag" icon
    3. navigate to `src/lib/regex.js` in the tree of the current release
    4. update the `emoji.js` script with the new regex
    5. update the source link in `emoji.js` for the new regex

2. Update the styles:
    1. open `emoji.js` and search for "`// Emoji styles`"
    2. go through the list of styles, open the source url, and check for new
       versions
    3. add any new versions to the beginning of the `versions` lists
       (the most recent version must be the first entry of the list)

3. Update the downloader:
    1. open `emoji-dl.sh` and search for "`declare -A supported_sets`"
    2. add any new versions to the metadata array as described in the script
    3. (you can use the scriptlet in `emoji.js` to generate this array, too)

After a new version has been added, users must update / re-download their styles
to be able to use them. Emojis from the old version will still be rendered
correctly even if the style has not been updated yet.


### Adding support for a new style

Supporting a new emoji style is generally pretty straight forward.

1. open [`emoji.js`](https://gitlab.com/whisperfish/whisperfish/-/blob/master/qml/js/emoji.js)
   and [`emoji-dl.sh`](emoji-dl.sh)
2. search in both files for all occurrences of "`>>> ADD NEW STYLES HERE <<<`"
   and add the new style analogous to the existing ones
3. open `Emojis.md` and add the new style to the overview table

Adding styles that are not downloaded from Emojipedia or a repository on Github
will require some understanding of the code.


### Architecture

Whisperfish and the patched emoji keyboard support different emoji styles. They
have to be installed in:

    ~/.local/share/sailor-emoji/

All styles have to be registered in [`qml/js/emoji.js`](https://gitlab.com/whisperfish/whisperfish/-/blob/master/qml/js/emoji.js).

Emoji sets are folders of emoji icons in either a vector or raster format (`svg`
is preferred). Each emoji icon must be named `<hex codepoint>.<ext>` (lower case),
combined codepoints are separated by `-`. Some emojis may include the "emoji
style" selector (`uFE0F`); it is recommended to create symbolic links for the
version with and without the selector.

Raster emojis must be available in the sizes 72x72px and 144x144px.
