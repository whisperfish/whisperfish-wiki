# Attachments

## Storage

The configuration file has a setting for the directory where incoming attachments will be stored. Have a look at the config file in `/home/<username>/.config/share/be.rubdos/harbour-whisperfish/`. You'll find the right variable. ;)

## Migration

If you want to move your attachments to another directory (e.g. the SD card) and want whisperfish to know where they are, you have to open the database with any means you like (see [Database](Database)) and execute the following SQL snippet. Be sure to set the old path (in the example starting with `/home`) and the new path (in the example starting with `/run`) accordingly. Additionally, be sure to replace the placeholders `<xxxx-xxxx>` and `<username>` according to your device.

    update "main"."attachments" set attachment_path = replace (attachment_path, '/home/<username>/.local/share/be.rubdos/harbour-whisperfish/storage/attachments/', '/run/media/<username>/<xxxx-xxxx>/whisperfish/attachments/')`
