Whisperfish can transcribe voice notes to text. This is a feature that is not available in the official Signal app. The transcription is done on the device itself, so no data is sent to a server. This feature is still in development and may not work perfectly.

It leverages the ["Speech Note"](https://openrepos.net/content/mkiol/speech-note)/[dsnote](https://github.com/mkiol/dsnote/blob/main/dbus/org.mkiol.Speech.xml) application.
To use the feature, install this application from the OpenRepos store,
and configure an English STT model.
The "tiny" model is recommended on all devices including and prior to the Xperia 10,
while the "base" model is recommended on the Xperia 10 II and newer devices.
The application does not need to be running, but it needs to be installed and configured.

To transcribe a voice note, open the voice note in Whisperfish, and press the "Transcribe" button. The transcription will be shown in the chat.
You can also select multiple voice notes to transcribe them all at once,
using the "text" icon.

Alternatively, you can enable the "Transcribe voice notes" setting in Whisperfish, which will automatically transcribe all voice notes when they are received.

Currently, only English is supported.
Language autodetection is not yet available in dsnote: https://github.com/mkiol/dsnote/issues/147
