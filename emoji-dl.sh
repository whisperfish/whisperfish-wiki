#!/bin/bash
#-
# emoji-dl 1.0.2 (2023-04-12)
# Copyright (C) 2022-2023  Mirian Margiani
# License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.
#·
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# ------------------------------------------------------------------------------
# CONFIGURATION

shopt -s nullglob

sizes=(72 144)         # not all sizes are required, having 72 and 144 should suffice

declare -A valid_sizes
valid_sizes[60]=x
valid_sizes[72]=x
valid_sizes[120]=x
valid_sizes[144]=x
valid_sizes[160]=x

shrink=true  # set this to 'true' to reduce the file size of the images using ImageMagick
crush=false  # same as $shrink, but use pngcrush instead of ImageMagick
             # warning: this is very slow and resource demanding!

outdir="sailor-emoji"  # parent directory of downloaded sets
tempdir="${outdir}-temporary"

declare -A supported_sets
# format:  [key in emoji.js] = type                 : current version : older version   : older version...
#                              → g for Github         → i.e. latest     → as specified
#                              → e for Emojipedia       supported         in emoji.js
#                                                       version
#
# note: use the scriptlet in emoji.js to generate this array!
#
supported_sets[openmoji]=g:15.0.0:14.0.0:13.1.0:13.0.0
supported_sets[twemoji]=g:15.1.0:14.0.2:13.0.1
supported_sets[microsoft-3D-fluent]=e:fluent-15.1
supported_sets[google]=e:16.0:15.1:15.0:android-12l
supported_sets[whatsapp]=e:2.24.2.76:2.23.2.72:2.22.8.79:2.21.23.23:2.21.16.20:2.21.11.17:2.20.206.24
supported_sets[apple]=e:ios-17.4:ios-16.4:ios-15.4:ios-14.6:ios-14.5:ios-14.2
# >>> ADD NEW STYLES HERE <<<

sets_to_download=()


# ------------------------------------------------------------------------------
# MAIN FUNCTIONS

function show_help() {
    printf -- "%s\n" "\
** Emoji Downloader **

This script downloads emoji sets and makes them ready to be used
in Whisperfish and the patched emoji keyboard for SailfishOS.

Usage:
    $(basename "$0") [-o OUT] [-x SIZES] [-S|-s] [-C|-c] SETS...

Example:
    $(basename "$0") openmoji

Arguments:
    SETS...           - one or more sets from the list below to download
                        You can either specify only the name to automatically
                        download the latest version, or manually specify
                        a specific version.
                        - \"openmoji\" will get you the latest version
                        - \"openmoji/13.0.0\" will get you version 13.0.0

    -o, --output OUT  - output directory (default: $outdir)
    -x, --sizes SIZES - which sizes to download for raster emojis
                        - default: $(printf -- "%s " "${sizes[@]}")
                        - available: $(printf -- "%s " "${!valid_sizes[@]}")
                        - note: provide a comma separated list (\"72,144\")
    -s, --shrink      - reduce the file size of the images using ImageMagick
                        - default: $shrink
                        - note: this has no effect for vector emojis but reduces
                          the size of raster emojis from ~120 MiB to ~30 MiB
    -S, --no-shrink   - disable shrinking images, see --shrink
    -c, --crush       - same as --shrink but using pngcrush
                        - default: $crush
                        - note: this saves only ~5 MiB on top of --shrink
                        - warning: this is slow and resource demanding! Do not
                          attempt to run this on your phone.
    -C, --no-crush    - disable crushing raster images, see --crush

    -h, --help        - show this help and exit
    -V, --version     - show version and copyright information

Supported sets:
$(for i in "${!supported_sets[@]}"; do
    printf -- "    - %s\n" "$i:"
    printf -- "%s\n" "$(cut -d: -f2- <<< "${supported_sets[$i]}" | tr ':' '\n' | sed -Ee "s:^:        └ $i/:g")"
    echo
done)

Dependencies:
    - basic: curl, tar, unzip

    - for --shrink:
        - mogrify from ImageMagick
        - GNU parallel

    - for --crush:
        - pngcrush
        - GNU parallel
"
}


function main() { # @: script args

    # PARSE ARGUMENTS

    while (( $# > 0 )); do
        case "$1" in
            -h|--help)
                show_help
                exit 0
            ;;
            -V|--version)
                sed -n '/^#-/,/^#·/p;/^#·/q' "$0" | sed '1d;$d;s/#[ ]*//g'
                exit 0
            ;;
            -o|--output)
                outdir="$2"
                [[ "$2" == -* ]] && outdir='' || shift
            ;;
            -x|--size)
                mapfile -t sizes <<<"$(tr ',' '\n' <<<"$2")"
                [[ "$2" == -* ]] && sizes=() || shift
            ;;
            -s|--shrink) shrink=true;;
            -S|--no-shrink) shrink=false;;
            -c|--crush) crush=true;;
            -C|--no-crush) crush=false;;
            -*) printf -- "%s\n" "error: unknown option $1" >&2
                exit 1
            ;;
            *) sets_to_download+=("$1");;
        esac
        shift
    done


    # VERIFY SETUP, PART 1

    require_dependencies \
        "the vital tool '%s' could not be found" \
        "the following vital tools could not be found" \
        "" \
        curl tar unzip || exit 128


    # VERIFY CONFIGURATION

    if (( ${#sets_to_download[@]} == 0 )); then
        printf -- "%s\n" "error: no sets to download defined" >&2
        exit 1
    fi

    if [[ -z "$outdir" || "$outdir" == -* ]]; then
        printf -- "%s\n" "error: no output directory defined" >&2
        exit 1
    fi

    if (( ${#sizes[@]} == 0 )); then
        printf -- "%s\n" "error: no sizes for raster images defined" >&2
        exit 1
    fi

    for i in "${sizes[@]}"; do
        if [[ -z "${valid_sizes[$i]}" ]]; then
            printf -- "%s\n" "error: invalid size '$i' for raster images" >&2
            exit 1
        fi
    done

    local sets_to_download_combined=()
    local downloading_raster_sets=false

    for i in "${sets_to_download[@]}"; do
        local set="${i%/*}"
        local meta="${supported_sets[$set]}"
        local version=''

        if [[ -z "$meta" ]]; then
            printf -- "%s\n" "error: cannot download unknown set '$i'" >&2
            exit 1
        fi

        if [[ "$i" == */* ]]; then
            version="${i#*/}"

            if [[ -z "$version" ]]; then
                printf -- "%s\n" "error: no version for set '$i' defined" >&2
                exit 1
            fi

            if [[ "$version" == *:* ]]; then
                printf -- "%s\n" "error: invalid version for set '$i'" >&2
                exit 1
            fi

            if [[ ":${meta#*:}" != *":$version" && ":${meta#*:}" != *":$version:"* ]]; then
                printf -- "%s\n" "error: cannot download unknown version for set '$i'" >&2
                exit 1
            fi
        else
            # select the latest version if no version was defined
            version="${meta#*:}"
            version="${version%%:*}"
        fi

        local type="${meta%%:*}"

        # >>> ADD NEW STYLES HERE <<<
        # >>> if a new type has to be defined <<<
        if [[ ! "$type" =~ ^[ge]$ ]]; then
            printf -- "%s\n" "bug: unknown how to handle type '$type' for $i" >&2
            exit 255
        fi

        # >>> ADD NEW STYLES HERE <<<
        # >>> update this check if sets from a different source than Emojipedia use raster images <<<
        if [[ "$type" == "e" ]] || false; then
            downloading_raster_sets=true
        fi

        sets_to_download_combined+=("$type:$set/$version")
    done


    # VERIFY SETUP, PART 2

    if [[ "$(cat /etc/issue)" == *Sailfish* ]]; then
        # The script should not be run under Sailfish for many reasons.
        # It has not been tested, it needs too much juice, and it might fail
        # because of busybox etc.
        if [[ "$shrink" == true || "$crush" == true ]]; then
            printf -- "%s\n" "warning: You appear to be running this script on a Sailfish device." >&2
            printf -- "%s\n" "         It is *strongly* recommended to disable shrinking (-S -C), or" >&2
            printf -- "%s\n" "         run the script on a more powerful device." >&2
            exit 128  # softly: "don't"
        else
            printf -- "%s\n" "warning: You appear to be running this script on a Sailfish device." >&2
            printf -- "%s\n" "         This has not been tested and is not recommended." >&2
            printf -- "%s\n" "         It may fail unexpectedly due to outdated and limited software." >&2
            printf -- "%s\n" "Continuing in 5 seconds..." >&2
            sleep 5  # give the user time to reconsider their life choices
        fi
    fi

    # Require raster tools only when downloading raster sets, and after
    # possibly nagging about Sailfish. This might prevent people from trying to
    # install tools on their phone, only to be greeted by a giant warning message.
    if [[ "$downloading_raster_sets" == true ]] && [[ "$shrink" == true || "$crush" == true ]]; then
        require_dependencies \
            "the tool '%s' could not be found" \
            "the following tools could not be found" \
            "This is required for compressing images.\nEither make sure all tools are available in the \$PATH, or\ndisable compression using --no-shrink and --no-crush. Then retry." \
            parallel mogrify pngcrush || exit 128
    fi


    # PREPARE OUTPUT DIRECTORIES

    mkdir -p "$outdir" || {
        printf -- "%s\n" "error: failed to create output directory '$outdir'" >&2
        rmdir --ignore-fail-on-non-empty "$outdir"
        exit 2
    }

    tempdir="${outdir}-temporary"
    mkdir -p "$tempdir" || {
        printf -- "%s\n" "error: failed to create temporary directory '$tempdir'" >&2
        rmdir --ignore-fail-on-non-empty "$outdir" "$tempdir"
        exit 2
    }

    sets_to_download=()
    for i in "${sets_to_download_combined[@]}"; do
        local out_temp="$tempdir/${i#*:}"
        mkdir -p "$out_temp" || {
            printf -- "%s\n" "error: failed to create directory '$out_temp'" >&2
            printf -- "%s\n" "warning: skipping $i" >&2
            rmdir --ignore-fail-on-non-empty "$outdir" "$tempdir"
            continue
        }

        local out_save="$outdir/${i#*:}"
        mkdir -p "$out_save" || {
            printf -- "%s\n" "error: failed to create directory '$out_save'" >&2
            printf -- "%s\n" "warning: skipping $i" >&2
            rmdir --ignore-fail-on-non-empty "$outdir" "$tempdir" "$out_temp"
            continue
        }

        sets_to_download+=("$i")
    done


    # DOWNLOAD EMOJIS

    for i in "${sets_to_download[@]}"; do
        local setver="${i#*:}"
        local set="${setver%%/*}"
        local version="${setver#*/}"

        printf -- "%s\n" "getting $set, version $version..." >&2

        case "$i" in
            # >>> ADD NEW STYLES HERE <<<
            # >>> if a new type has to be defined <<<
            g:*) get_from_github "$set" "$version" || {
                printf -- "%s\n" "error: failed to download version $version of set $set" >&2
                continue
            };;
            e:*) get_from_emojipedia "$set" "$version" || {
                printf -- "%s\n" "error: failed to download version $version of set $set" >&2
                continue
            };;
        esac
    done


    # TODO: CREATE AN ARCHIVE
    # Maybe create a tar.gz archive that already contains the right folder
    # structure, so users can simply extract it in $HOME and be done with it.
}


# ------------------------------------------------------------------------------
# HELPER FUNCTIONS

function require_dependencies() {  # 1: error message (singular, use '%s' for the command)
                                   # 2: error message (plural, don't mention commands)
                                   # 3: explanation
                                   # 4..: commands to check
    local error_singular="$1"
    local error_plural="$2"
    local error_explanation="$3"
    shift 3
    local required=("$@")
    local missing=()

    for i in "${required[@]}"; do
        which "$i" >/dev/null 2>&1 || missing+=("$i")
    done

    if (( ${#missing[@]} == 1 )); then
        printf -- "error: $error_singular\n" "${missing[0]}" >&2
    elif (( ${#missing[@]} > 1 )); then
        printf -- "error: %s\n" "$error_plural" >&2
        printf -- "           - %s\n" "${missing[@]}" >&2
    fi

    if (( ${#missing[@]} != 0 )); then
        if [[ -n "$error_explanation" ]]; then
            printf -- "\n$error_explanation\n" "" >&2
        else
            printf -- "\n%s\n" "Make sure all tools are installed and available in the \$PATH, then retry." >&2
        fi

        return 128
    fi

    return 0
}


function shrink_image() {  # 1: file
    [[ ! -e "$1" || -h "$1" ]] && return 0  # don't process linked files twice

    if [[ "$shrink" == true ]]; then
        mogrify -depth 4 -format png "$1" || {
            ret=$?
            printf -- "%s\n" "warning: mogrify failed for $1 with code $ret" >&2
            return $ret
        }
    fi

    if [[ "$crush" == true ]]; then
        pngcrush -q -brute -ow "$1" "$1-tmp.png" 2>/dev/null || {
            ret=$?
            printf -- "%s\n" "warning: pngcrush failed for $1 with code $ret" >&2
            return $ret
        }
    fi
}


function curl_get() {  # @: extra arguments
    if (( "$#" == 0 )); then
        printf -- "%s\n" "bug: curl_get requires at least an URL to fetch" >&2
        return 255
    fi

    curl \
        -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
        -H 'Accept-Language: en-US;q=0.7,en;q=0.3' \
        -H 'DNT: 1' \
        -H 'Connection: keep-alive' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'Sec-GPC: 1' \
        -H 'Pragma: no-cache' \
        -H 'Cache-Control: no-cache' \
        --compressed "$@"
}


# ------------------------------------------------------------------------------
# GETTER FUNCTIONS

function get_from_github() {  # 1: set, 2: version

    # PREPARATION

    local set="$1"
    local version="$2"
    local out_temp="$tempdir/$set/$version"
    local out_save="$outdir/$set/$version"

    local files_dir="$out_temp/files"
    mkdir -p "$files_dir"

    local url=""
    local suffix=""


    # DEFINE DOWNLOAD URL AND TARGET ARCHIVE FILE NAME SUFFIX
    # must define $url and $suffix

    case "$set" in
        openmoji)
            url="https://github.com/hfg-gmuend/openmoji/releases/download/${version}/openmoji-svg-color.zip"
            suffix="zip"
        ;;
        twemoji)
            url="https://github.com/jdecked/twemoji/archive/refs/tags/v${version}.tar.gz"
            suffix="tar.gz"
        ;;
        # >>> ADD NEW STYLES HERE <<<
        *)  printf -- "%s\n" "bug: cannot preapre unknown set '$set' for download" >&2
            exit 255
        ;;
    esac


    # DOWNLOAD THE ARCHIVE

    local archive="$out_temp/$set-$version.$suffix"

    if [[ -f "$archive" ]]; then
        printf -- "%s\n" "note: skipping download of cached archive for '$set'" >&2
    else
        rm -f "$archive" || true
        curl_get -L "$url" -o "$archive"
    fi


    # EXTRACT THE ARCHIVE FROM $archive TO $files_dir
    # the target directory $files_dir must contain all SVG files afterwards

    case "$set" in
        openmoji)
            unzip -qq "$archive" -d "$files_dir"
        ;;
        twemoji)
            tar -xzf "$archive" --strip-components=3 -C "$files_dir" --wildcards "$set-$version/assets/svg"
        ;;
        # >>> ADD NEW STYLES HERE <<<
        *)  printf -- "%s\n" "bug: cannot extract archive for unknown set '$set'" >&2
            exit 255
        ;;
    esac


    # CLEANUP AND INSTALLATION
    # files will be moved from $files_dir to $out_save

    pushd "$files_dir" >/dev/null

    printf -- "%s\n" "renaming files..." >&2
    for i in *.svg; do
        local lower="$(echo "$i" | tr '[:upper:]' '[:lower:]')"
        [[ "$i" != "$lower" ]] && mv "$i" "$lower"
    done

    printf -- "%s\n" "linking alternatives..." >&2
    for i in *-fe0f.svg; do
        local target="${i%-fe0f.svg}.svg"
        [[ ! -f "$target" ]] && ln -b -s "$i" "$target"
    done

    popd >/dev/null

    mv "$files_dir/"*.svg -t "$out_save"
    rmdir "$files_dir"

    return 0
}


function get_from_emojipedia() {  # 1: set, 2: version

    # PREPARATION

    local set="$1"
    local version="$2"
    local out_temp="$tempdir/$set/$version"
    local out_save="$outdir/$set/$version"

    pushd "$out_temp" >/dev/null


    # LINK LIST PREPARATION

    local url="https://emojipedia.org/$set/$version/"
    printf -- "%s\n" "fetching list of emojis: $set ($version)..." >&2

    local json="$set-$version.json"

    if [[ ! -f "$json" ]]; then
        local query="$(cat <<-"EOF"
			query vendorHistoricEmojiV1(
			    $slug: Slug!
			    $version: Slug = null
			    $status: VendorHistoricEmojiStatus = null
			    $lang: Language
			) {
			    vendorHistoricEmoji_v1(slug: $slug, version: $version, status: $status, lang: $lang) {
			        ...vendorHistoricEmojiResource
			    }
			}

			fragment vendorHistoricEmojiImageFragment on VendorHistoricEmojiImage {
			    slug
			    image {
			        source
			        description
			        useOriginalImage
			        isMonochrome
			    }
			    status
			}

			fragment vendorHistoricEmojiResource on VendorHistoricEmoji {
			    items {
			        category {
			            slug
			            title

			            representingEmoji {
			                code
			            }
			        }
			        images {
			            ...vendorHistoricEmojiImageFragment
			        }
			    }
			    statuses
			}
			EOF
        )"

        local data="$(cat <<-EOF
			{
				"query": "$query",
				"variables": {
				    "slug": "$set",
				    "version": "$version",
				    "lang": "EN"
				},
				"operationName": "vendorHistoricEmojiV1"
			}
			EOF
        )"

        curl_get \
            'https://emojipedia.org/api/graphql' \
            -X POST \
            -H 'content-type: application/json' \
            --data-raw "$(tr -d '\n' <<<"$data")" > "$json"
    fi


    # extract relative source urls and add host,
    # then format the list for 'curl -K'
    local host='https://em-content.zobj.net'
    sed 's/[,{}]/\n/g' "$json" |\
        grep -oe 'source/.*\.png' |\
        sed "s@^@$host/@g" |\
        sed 's/^/url="/g' |\
        sed 's/$/"/g' \
            > links_source

    if (( "$(wc -c < "links_source")" == 0 )); then
        printf -- "%s\n" "no links found for $set ($version), aborting" >&2
        return
    fi


    # DOWNLOAD

    printf -- "%s\n" "fetching images for $set ($version)..." >&2

    for size in "${sizes[@]}"; do
        printf -- "%s\n" "downloading ${size}x${size}px of $set ($version)..."

        # generate the url list for this size
        sed "s@/source/@/thumbs/$size/@g" "links_source" > "links_$size"

        # fetch files in the required resolutions
        mkdir -p "$size"

        pushd "$size" >/dev/null

        curl_get --parallel --parallel-max 15 --remote-name-all -K "../links_$size"

        # rename images from "<literal name>_[<skin tone>_]<hex codepoint>[_<skin tone modifier hex>].png" to "<hex codepoint>.png"
        # example: "waving-hand_dark-skin-tone_1f44b-1f3ff_1f3ff.png" to "1f44b-1f3ff.png"
        printf -- "%s\n" "fixing file names and alternatives..." >&2

        shopt -s nullglob
        shopt -s extglob

        function mv_if_ok() { # 1: from, 2: to
            [[ "$1" == "$2" ]] && return 0
            mv "$1" "$2"
        }

        for i in *.png; do
            # rename from "<literal name>_<stuff>.png" to "<stuff>.png"
            mv_if_ok "$i" "${i#*_}"
        done

        for i in *-skin-tone_*.png; do
            # remove the literal skin tone name from all skin tone variations
            # i.e. "<name>_<tone>-skin-tone_<hex>[_<stuff>].png" to "<hex>[_<stuff>].png"
            mv_if_ok "$i" "${i#*-skin-tone_}"
        done

        for i in *emoji-modifier-fitzpatrick-type-{1..6}*.png; do
            # some old emoji versions use this instead of "skin tone" as addition
            # to the literal name; remove it
            mv_if_ok "$i" "${i#*emoji-modifier-fitzpatrick-type-[1-6]?(-[1-6])_}"
        done

        for i in *_1f3f*.png; do
            # some emojis with skin tone variations have the "skin tone modifier" (u1F3Fx)
            # duplicated at the end of the file name
            # i.e. "<hex codepoint>_<skin tone modifier>.png" to "<hex codepoint>.png"
            mv_if_ok "$i" "${i%_1f3f*.png}.png"
        done

        for i in *-fe0f.png; do
            # some emojis include the "emoji style selector" (uFE0F)
            # We create symbolic links for the version with and without the selector,
            # as both versions are valid.
            ln -b -s "$i" "${i%-fe0f.png}.png"
        done

        popd >/dev/null
    done


    # COMPRESSION

    export shrink
    export crush
    export -f shrink_image

    if [[ "$shrink" == true || "$crush" == true ]]; then
        printf -- "%s\n" "compressing images..." >&2

        for size in "${sizes[@]}"; do
            tar -czf "$size.large.tar.gz" "$size"
        done

        { find "${sizes[@]}" -name "*.png" -type f -print0 | parallel -0 --bar --memsuspend 200M --memfree 200M -n 1 --retries 1 shrink_image; } || {
            echo "error: failed to shrink images" >&2
        }

        for size in "${sizes[@]}"; do
            tar -czf "$size.small.tar.gz" "$size"
        done
    fi


    # CLEAN UP AND INSTALLATION

    popd >/dev/null

    for size in "${sizes[@]}"; do
        mv "$out_temp/$size" -t "$out_save"
    done

    return 0
}

# >>> ADD NEW STYLES HERE <<<
# >>> if necessary: place new getter functions here <<<
# >>> they must be named get_from_<source> <<<


# ------------------------------------------------------------------------------
# RUNNING THE MAIN FUNCTION

main "$@"
exit 0
