> **Important:** If you updated your active Whisperfish installation and it greets you with a registration prompt, do not continue! See chapter "Migration issues" below!

# Sailjail and user data paths

When [Jolla introduced sandboxing](https://blog.jolla.com/whats-up-with-sandboxing/), aka. Sailjail, it changed the applications' user data paths. Jolla left [migrating the applications' user data](https://forum.sailfishos.org/t/migrating-configuration-and-data-files-for-sandboxed-apps/8866) to the new paths to the developers (which is a good thing, actually). Whisperfish created some empty config to the new path at once, so the rest had to follow.

Whisperfish tries its best to detect the need for migration, and performs it automatically at startup. A simplified summary of the logic in beta.11:

- If the new paths exist, do nothing (already correct).
- If nothing exists, create the new folders (new installation).
- If the old paths exist, but the new ones don't, migrate them (the expected case).
- If some other mixture of old and new paths exist, abort startup (uh-oh).

# Migration issues

If Whisperfish wants you to re-register after updating Whisperfish, it means that the migration either wasn't complete, or that it wasn't deemed necessary. Please run the script below and attach the results file to @direc85 over at [#whisperfish Matrix channel](https://matrix.to/#/#whisperfish:rubdos.be), or create [a new issue](https://gitlab.com/whisperfish/whisperfish/-/issues/new), or post it to the [Sailfish OS forum Whisperfish thread](https://forum.sailfishos.org/t/whisperfish-the-unofficial-sailfishos-signal-client/3337).

The same applies if Whisperfish doesn't start at all (the application cover shows up spinning and disappears).

```bash
#!/bin/sh

export LANG=C
export LC_ALL=C

DATE=`date +%Y%m%d_%H%M%S`
LOG="whisperfish-paths_$DATE.log"
OLD_CONF_DIR=.config/harbour-whisperfish
OLD_DATA_DIR=.local/share/harbour-whisperfish
NEW_CONF_DIR=.config/be.rubdos/harbour-whisperfish
NEW_DATA_DIR=.local/share/be.rubdos/harbour-whisperfish
WF=harbour-whisperfish

cd

# Get the OS version etc.
source /etc/sailfish-release

echo "Sailfish: $VERSION" > "$LOG"
rpm -qi harbour-whisperfish >> "$LOG"

echo -e "\nNew directories" >> "$LOG"
find "$NEW_CONF_DIR/" -type d -exec ls -ld {} \; >> "$LOG" 2>> "$LOG"
find "$NEW_DATA_DIR/" -type d -exec ls -ld {} \; >> "$LOG" 2>> "$LOG"

echo -e "\nOld directories" >> "$LOG"
find "$OLD_CONF_DIR/" -type d -exec ls -ld {} \; >> "$LOG" 2>> "$LOG"
find "$OLD_DATA_DIR/" -type d -exec ls -ld {} \; >> "$LOG" 2>> "$LOG"

echo -e "\nNew files" >> "$LOG"
ls -l "$NEW_CONF_DIR/$WF.conf" >> "$LOG" 2>> "$LOG"
ls -l "$NEW_CONF_DIR/config.yml" >> "$LOG" 2>> "$LOG"
ls -l "$NEW_DATA_DIR/db/$WF.db" >> "$LOG" 2>> "$LOG"
NUM_PREKEYS=`ls -1 "$NEW_DATA_DIR/storage/prekeys" 2>/dev/null | wc -l`
echo "Number of prekeys: $NUM_PREKEYS" >> "$LOG"

echo -e "\nOld config files" >> "$LOG"
ls -l "$OLD_CONF_DIR/$WF.conf" >> "$LOG" 2>> "$LOG"
ls -l "$OLD_CONF_DIR/config.yml" >> "$LOG" 2>> "$LOG"
ls -l "$OLD_DATA_DIR/db/$WF.db" >> "$LOG" 2>> "$LOG"
NUM_PREKEYS=`ls -1 "$OLD_DATA_DIR/storage/prekeys" 2>/dev/null | wc -l`
echo "Number of prekeys: $NUM_PREKEYS" >> "$LOG"

echo -e "\nAll '$WF.conf' files" >> "$LOG"
find -name "$WF.conf" -type f >> "$LOG" 2>/dev/null

echo -e "\nAll 'config.yml' files" >> "$LOG"
find -name "config.yml" -type f >> "$LOG" 2>/dev/null

echo -e "\nAll '$WF.db' files" >> "$LOG"
find -name "$WF.db" -type f >> "$LOG" 2>/dev/null

echo ""
echo "$LOG saved, please review it and send it to direc85"
echo ""
echo "Please send part of the startup log under Sailjail too,"
echo "but I only need up to and including line that says:"
echo "  'QmlApp::application loaded'"
echo ""
echo "Instructions for how to grab the Sailjail logs:"
echo "  https://gitlab.com/whisperfish/whisperfish/-/wikis/home#logging-with-sailjail"
echo ""
```